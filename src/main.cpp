#include <stdio.h>
#include "hardware/pwm.h"
#include "pico/stdlib.h"
#include "mcp2515/mcp2515.h"

// Params //////////////////////////////////////////////////////////////////////

// #define DEBUG

// User Config
#define VOLUME_DEFAULT  6U
#define BASS_DEFAULT    9U
#define LEVEL_RANGE     32U
#define CAN_TIMEOUT     1000000U  // ~5 seconds, determined empirically
#define WAKEUP_DELAY    5000U     // ms

// Hardware Config
#define PIN_FANS        28U
#define PIN_UPSTREAM    27U
#define PIN_DOWNSTREAM  26U
#define PIN_WAKEUP      22U
#define PIN_VOLUME      21U
#define PIN_BASS        20U
#define PWM_RANGE       0xFFFF
#define WHEEL_RANGE     64U
#define MESSAGE_ID_DISPLAY_ON   0x0353
#define MESSAGE_ID_SCROLL_WHEEL 0x03C2

// Global Variables ////////////////////////////////////////////////////////////
MCP2515 _can_;
struct can_frame _rx_;
uint32_t _volume_{VOLUME_DEFAULT};
uint32_t _bass_{BASS_DEFAULT};
bool _awake_{false};

// Prototypes //////////////////////////////////////////////////////////////////

void initialize_can(void);
void initialize_dsp(void);
void initialize_wakeup(void);
bool read_can(void);
void update_dsp(void);
void update_wakeup(void);
uint32_t update_level(const uint32_t level, const uint32_t wheel_position);
void wait_for_charge(void);

// Main ////////////////////////////////////////////////////////////////////////

int main() {
  #ifdef DEBUG
    stdio_init_all();
  #endif

  uint32_t can_stale_count{0U};
  bool received_can{false};
  
  initialize_can();
  initialize_wakeup();
  initialize_dsp();
  wait_for_charge();

  while(true) {
    received_can = read_can();

    can_stale_count = received_can ? 0U : (can_stale_count + 1U);

    if(can_stale_count > CAN_TIMEOUT) {
      _awake_ = false;
      _volume_ = VOLUME_DEFAULT;
      _bass_ = BASS_DEFAULT;

      #ifdef DEBUG
        printf("Timeout\n");
      #endif
    }

    update_wakeup();
    update_dsp();
  }

  return 0;
}

// Local Functions /////////////////////////////////////////////////////////////

#define GET_BIT(byte, bit_num)  (((byte) & (1 << (bit_num))) >> (bit_num))

void initialize_can(void) {  
  _can_.reset();
  _can_.setBitrate(CAN_500KBPS, MCP_8MHZ);
  _can_.setNormalMode();
}

void initialize_wakeup(void) {
  gpio_init(PIN_WAKEUP);
  gpio_set_dir(PIN_WAKEUP, GPIO_OUT);
  gpio_put(PIN_WAKEUP, false);

  gpio_init(PIN_FANS);
  gpio_set_dir(PIN_FANS, GPIO_OUT);
  gpio_put(PIN_FANS, false);

  gpio_init(PICO_DEFAULT_LED_PIN);
  gpio_set_dir(PICO_DEFAULT_LED_PIN, GPIO_OUT);
  gpio_put(PICO_DEFAULT_LED_PIN, false);
}

void initialize_dsp(void) {
  gpio_set_function(PIN_VOLUME, GPIO_FUNC_PWM);
  uint slice_num = pwm_gpio_to_slice_num(PIN_VOLUME);
  pwm_config config = pwm_get_default_config();
  pwm_config_set_clkdiv(&config, 4.0F);
  pwm_init(slice_num, &config, true);
  uint32_t duty = PWM_RANGE * VOLUME_DEFAULT / LEVEL_RANGE;
  pwm_set_gpio_level(PIN_VOLUME, duty);

  gpio_set_function(PIN_BASS, GPIO_FUNC_PWM);
  slice_num = pwm_gpio_to_slice_num(PIN_BASS);
  config = pwm_get_default_config();
  pwm_config_set_clkdiv(&config, 4.0F);
  pwm_init(slice_num, &config, true);
  duty = PWM_RANGE * BASS_DEFAULT / LEVEL_RANGE;
  pwm_set_gpio_level(PIN_BASS, duty);
}

bool read_can(void) {
  if (_can_.readMessage(&_rx_) == MCP2515::ERROR_OK) {
    // Scroll Message
    if((_rx_.can_id == MESSAGE_ID_SCROLL_WHEEL) && (GET_BIT(_rx_.data[0U], 5U) == 1U)) {
      _volume_ =
        !_awake_ ?
          VOLUME_DEFAULT :
          update_level(_volume_, (_rx_.data[2U] & 0b00111111));

      const bool right_button = GET_BIT(_rx_.data[1U], 5U);
      _bass_ =
        (right_button || !_awake_) ?
          BASS_DEFAULT :
          update_level(_bass_, (_rx_.data[3U] & 0b00111111));

      #ifdef DEBUG
        printf("Volume %u Bass %u\n", _volume_, _bass_);
      #endif

      return true;
      
    // Display Message
    } else if (_rx_.can_id == MESSAGE_ID_DISPLAY_ON) {
      _awake_ = GET_BIT(_rx_.data[0U], 5U);

      if (!_awake_) {
        _volume_ = VOLUME_DEFAULT;
        _bass_ = BASS_DEFAULT;
      }

      #ifdef DEBUG
        printf("Wake %i\n", _awake_);
      #endif

      return true;
    }
  }

  return false;
}

void update_dsp(void) {
  uint32_t duty = PWM_RANGE * _volume_ / LEVEL_RANGE;
  pwm_set_gpio_level(PIN_VOLUME, duty);

  duty = PWM_RANGE * _bass_ / LEVEL_RANGE;
  pwm_set_gpio_level(PIN_BASS, duty);
}

uint32_t update_level(const uint32_t level, const uint32_t wheel_position) {
  return
    // is change positive?
    (wheel_position < (WHEEL_RANGE / 2U)) ?
      // change is positive
      (((level + wheel_position) < LEVEL_RANGE) ?
        (level + wheel_position) :
        LEVEL_RANGE) :
      // change is negative
      ((level > (WHEEL_RANGE - wheel_position)) ?
        (level + wheel_position - WHEEL_RANGE) :
        0U);
};

void update_wakeup(void) {
  gpio_put(PIN_WAKEUP, _awake_);
  gpio_put(PIN_FANS, _awake_);
  gpio_put(PICO_DEFAULT_LED_PIN, _awake_);
}

void wait_for_charge(void) {
  sleep_ms(WAKEUP_DELAY);
}

// End of file /////////////////////////////////////////////////////////////////
